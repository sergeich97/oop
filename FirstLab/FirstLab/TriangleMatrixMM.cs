﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public partial class TriangleMatrix
    {
        public override void MMDiag(StreamWriter write)
        {
            write.WriteLine("Diagonal and Triangle");
        }

        public override void MMSquare(StreamWriter write)
        {
            write.WriteLine("Square && Triangle");
        }

        public override void MMTriangle(StreamWriter write)
        {
            write.WriteLine("Triangle x 2");
        }

        public override void MultiMethod(Matrix b, StreamWriter write)
        {
            b.MMTriangle(write);
        }
    }
}
