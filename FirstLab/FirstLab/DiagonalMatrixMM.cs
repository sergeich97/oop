﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public partial class DiagonalMatrix
    {
        public override void MMDiag(StreamWriter write)
        {
            write.WriteLine("two Diagonals");
        }

        public override void MMSquare(StreamWriter write)
        {
            write.WriteLine("Square wth Diagonal");
        }

        public override void MMTriangle(StreamWriter write)
        {
            write.WriteLine("Triangle with Diagonal");
        }

        public override void MultiMethod(Matrix b, StreamWriter write)
        {
            b.MMDiag(write);
        }
    }
}
