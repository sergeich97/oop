﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public partial class SquareMatrix
    {
        public SquareMatrix(int size)
        {
            Size = size;
            Matr = new int[Size, Size];
        }
    }
}
