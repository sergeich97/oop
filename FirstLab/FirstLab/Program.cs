﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                StreamReader read = new StreamReader(args[0]);
                StreamWriter write = new StreamWriter(args[1]);
                StreamWriter mult = new StreamWriter("D:/multi.txt");
                var container = new Container();
                Console.WriteLine("Start");
                container.In(read);
                if (container.Length > 0)
                {
                    Console.WriteLine("Matrices were entered");
                    container.Out(write);
                    container.Multi(mult);
                    Console.WriteLine("Matrices were printed");
                    container.GetSum(write);
                    container.Sort();
                    container.Out(write);
                    write.WriteLine("Filtered OUt");

                    container.FilteredOut(write);
                    container.Erase();
                    container.Out(write);
                    mult.Close();
                    Console.WriteLine("Container was erased");
                    Console.WriteLine("Stop");
                }
                else
                {
                    Console.WriteLine("Ошибка чтения файла");
                }
                read.Close();
                write.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка открытия файла. Файл не существует");                
            }
            Console.ReadKey();
        }
    }
}
