﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public partial class Container
    {
        public void GetSum(StreamWriter write)
        {
            for (int i = 0; i < Length; i++)
            {
                write.WriteLine("Sum of elements Matrix #{0} = {1}",i+1,Massiv[i].GetSum());
            }
        }
    }
}
