﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public partial class DiagonalMatrix
    {
        public override void Out(StreamWriter write)
        {
            write.WriteLine("This is DiagonalMatrix with the Size = {0}", Size);

            switch (OutType)
            {
                case FormatOut.row:
                    {
                        write.WriteLine("Printing by each row");
                        for (int i = 0; i < Size; i++)
                        {
                            for (int j = 0; j < Size; j++)
                            {
                                if (i == j)
                                    write.Write("{0} ", Diag[i]);
                                else
                                    write.Write("0 ");
                            }
                            write.WriteLine();
                        }
                        break;
                    }
                case FormatOut.column:
                    {
                        write.WriteLine("Printing by each column");
                        for (int i = 0; i < Size; i++)
                        {
                            for (int j = 0; j < Size; j++)
                            {
                                if (i == j)
                                    write.Write("{0} ", Diag[i]);
                                else
                                    write.Write("0 ");
                            }
                            write.WriteLine();
                        }
                        break;
                    }
                case FormatOut.inline:
                    {
                        write.WriteLine("Printing inline");
                        for (int i = 0; i < Size; i++)
                        {
                            for (int j = 0; j < Size; j++)
                            {
                                if (i == j)
                                    write.Write("{0} ", Diag[i]);
                                else
                                    write.Write("0 ");
                            }    
                        }
                        break;
                    }
                default:
                    break;
            }
            write.WriteLine();
          
        }
    }
}
