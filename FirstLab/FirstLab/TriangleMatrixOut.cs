﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public partial class TriangleMatrix
    {
        public override void Out(StreamWriter write)
        {
            write.WriteLine("This is TriangleMatrix with the Size = {0}", Size);
            int h = 0;
            for(int i=0; i<Size; i++)
            {
                for(int j=0; j<Size; j++)
                {
                    if(j<=i)
                    {
                        write.Write("{0} ", Triangle[h]);
                        h++;
                    }
                    else
                    {
                        write.Write("0 ");
                    }
                }
                write.WriteLine();
            }       
        }
    }
}
