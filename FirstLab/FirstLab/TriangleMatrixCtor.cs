﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public partial class TriangleMatrix
    {
        public TriangleMatrix(int size)
        {
            Size = size;
            Triangle = new int[((1+Size)*Size)/2];
        }
    }
}
