﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public partial class DiagonalMatrix
    {
        public override int GetSum()
        {
            int sum = 0;
            for (int i = 0; i < Size; i++)
            {
                sum += Diag[i];
            }
            return sum;
        }
    }
}
