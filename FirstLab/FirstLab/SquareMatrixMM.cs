﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public partial class SquareMatrix
    {
        public override void MMDiag(StreamWriter write)
        {
            write.WriteLine("Diagonal vs Square");
        }

        public override void MMSquare(StreamWriter write)
        {
            write.WriteLine("2 x Square");
        }

        public override void MMTriangle(StreamWriter write)
        {
            write.WriteLine("Triangle aand Square");
        }

        public override void MultiMethod(Matrix b, StreamWriter write)
        {
            b.MMSquare(write);
        }
    }
}
