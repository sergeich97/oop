﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public abstract partial  class Matrix:IComparable
    {
        public int Size { get; set; }

        public int CompareTo(object obj)
        {
            var second = obj as Matrix;

            if(this.GetSum()<second.GetSum() )
            {
                return -1;
            }
            else
            {
                if(this.GetSum()>second.GetSum() )
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
