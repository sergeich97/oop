﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public partial class TriangleMatrix:Matrix
    {
        public override int GetSum()
        {
            int sum = 0;
            
                for (int j = 0; j < ((1 + Size) * Size) / 2; j++)
                {
                    sum += Triangle[j];
                }
            
            return sum;
        }
    }
}
