﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public partial class DiagonalMatrix
    {
        public DiagonalMatrix(int size)
        {
            Size = size;
            Diag = new int[Size];
        }
    }
}
