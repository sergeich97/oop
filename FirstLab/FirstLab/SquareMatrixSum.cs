﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public partial class SquareMatrix
    {
        public override int GetSum()
        {
            int sum = 0;
            for(int i=0; i<Size; i++)
            {
                for(int j=0; j<Size; j++)
                {
                    sum += Matr[i, j];
                }
            }
            return sum;
        }
    }
}
