﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public partial class SquareMatrix
    {
        public void In(StreamReader read)
        {
            for (int i = 0; i < Size; i++)
            {
                var st = read.ReadLine();
                var ar = st.Split(' ').ToArray();
                for (int j = 0; j < Size; j++)
                {
                    try
                    {
                        Matr[i, j] = Convert.ToInt32(ar[j]);
                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine(ex);
                        Matr = null;
                        break;
                    }
                   
                }
            }
        }
    }
}
