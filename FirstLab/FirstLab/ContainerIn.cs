﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public partial class Container
    {
        public void In(StreamReader read)
        {
            try
            {
                var n = Convert.ToInt32(read.ReadLine());
                Massiv = new Matrix[n];
                for (int i = 0; i < n; i++)
                {
                    var matrix = Matrix.In(read);
                    if (matrix != null)
                    {
                        if (Length < 100)
                        {
                            Massiv[Length] = matrix;
                            Length++;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Ошибка входных данных");
                        Massiv = null;
                        Length = 0;
                        break;
                    }
                }
                var tmp = Massiv;
                Massiv = new Matrix[Length];
                for (int i = 0; i < Length; i++)
                {
                    Massiv[i] = tmp[i];
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            
        }
    }
}
