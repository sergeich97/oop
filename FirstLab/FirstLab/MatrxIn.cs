﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public abstract partial class Matrix
    {
        public static Matrix In(StreamReader read)
        {
            try
            {
                var st = read.ReadLine();
                var arr = st.Split(' ').ToArray();
                var size = Convert.ToInt32(arr[1]);
                var en = Convert.ToInt32(arr[2]);
                var ind = Convert.ToInt32(arr[0]);
                if (size <= 0 || en < 0 || en > 2 || ind < 0 || ind > 2)
                {
                    return null;
                }
                if (ind == 0)
                {
                    var diag = new DiagonalMatrix(size);
                    diag.In(read);
                    if (diag.Diag != null)
                    {


                        switch (en)
                        {
                            case 0:
                                {
                                    diag.OutType = FormatOut.row;
                                    break;
                                }
                            case 1:
                                {
                                    diag.OutType = FormatOut.column;
                                    break;
                                }
                            case 2:
                                {
                                    diag.OutType = FormatOut.inline;
                                    break;
                                }
                            default:
                                break;
                        }
                        return diag as Matrix;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {

                    if (Convert.ToInt32(arr[0]) == 1)
                    {
                        var square = new SquareMatrix(size);
                        square.In(read);
                        if (square.Matr != null)
                        {
                            switch (en)
                            {
                                case 0:
                                    {
                                        square.OutType = FormatOut.row;
                                        break;
                                    }
                                case 1:
                                    {
                                        square.OutType = FormatOut.column;
                                        break;
                                    }
                                case 2:
                                    {
                                        square.OutType = FormatOut.inline;
                                        break;
                                    }
                                default:
                                    break;
                            }


                            return square as Matrix;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        var triangle = new TriangleMatrix(size);
                        triangle.In(read);
                        if (triangle.Triangle != null)
                        {
                            switch (en)
                            {
                                case 0:
                                    {
                                        triangle.OutType = FormatOut.row;
                                        break;
                                    }
                                case 1:
                                    {
                                        triangle.OutType = FormatOut.column;
                                        break;
                                    }
                                case 2:
                                    {
                                        triangle.OutType = FormatOut.inline;
                                        break;
                                    }
                                default:
                                    break;
                            }
                            return triangle as Matrix;
                        }
                        else
                        {
                            return null;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
           
        }
    }
}
