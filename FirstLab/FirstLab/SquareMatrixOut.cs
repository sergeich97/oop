﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public partial class SquareMatrix
    {
        public override void Out(StreamWriter write)
        {
            write.WriteLine("This is a SquareMatrix with Size = {0}", Size);
            switch (OutType)
            {
                case FormatOut.row:
                    {
                        write.WriteLine("Printing by each row");
                        for (int i = 0; i < Size; i++)
                        {
                            for (int j = 0; j < Size; j++)
                            {
                                write.Write("{0} ", Matr[i, j]);
                            }
                            write.WriteLine();
                        }
                        break;
                    }
                case FormatOut.column:
                    {
                        write.WriteLine("Printing by each column");
                        for (int i = 0; i < Size; i++)
                        {
                            for (int j = 0; j < Size; j++)
                            {
                                write.Write("{0} ", Matr[i, j]);
                            }
                            write.WriteLine();
                        }
                        break;
                    }
                case FormatOut.inline:
                    {
                        write.WriteLine("Printing inline");
                        for (int i = 0; i < Size; i++)
                        {
                            for (int j = 0; j < Size; j++)
                            {
                                write.Write("{0} ", Matr[i, j]);
                            }                            
                        }
                        write.WriteLine();
                        break;
                    }
                default:
                    break;
            }

            
        }
    }
}
