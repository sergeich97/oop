﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public partial class DiagonalMatrix : Matrix
    {
        public int[] Diag { get; set; }
    }
}
