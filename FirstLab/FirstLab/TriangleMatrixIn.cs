﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    public partial class TriangleMatrix
    {
        public void In(StreamReader read)
        {
            var st = read.ReadLine();
            var ar = st.Split(' ').ToArray();
            for (int i = 0; i < ((1 + Size) * Size) / 2; i++)
            {
                try
                {
                    Triangle[i] = Convert.ToInt32(ar[i]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    Triangle = null;
                    break;                   
                }

            }
        }
    }
}
