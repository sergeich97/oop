﻿using System;
using System.IO;
using FirstLab;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void SquareSum()
        {
            var square = new SquareMatrix(3);
            int h = 0, sum = 0; ;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    square.Matr[i, j] = h;
                    sum += h;
                    h++;
                }
            }
            Assert.AreEqual(square.GetSum(), sum);
        }

        [TestMethod]
        public void DiagSum()
        {
            var diag = new DiagonalMatrix(3);
            int h = 0, sum = 0; ;
            for (int i = 0; i < 3; i++)
            {

                diag.Diag[i] = h;
                sum += h;
                h++;

            }
            Assert.AreEqual(diag.GetSum(), sum);
        }

        [TestMethod]
        public void TriangleSum()
        {
            var triag = new TriangleMatrix(3);
            int h = 0, sum = 0; ;
            for (int i = 0; i < 6; i++)
            {

                triag.Triangle[i] = h;
                sum += h;
                h++;

            }
            Assert.AreEqual(triag.GetSum(), sum);
        }


        [TestMethod]
        public void SquareIn()
        {
            var sq = new SquareMatrix(3);
            int h = 0;
            var wr = new StreamWriter("D:/test.in");
            wr.WriteLine("0 3 1");
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    sq.Matr[i, j] = h;
                    wr.Write("{0} ", sq.Matr[i, j]);
                    h++;
                }
                wr.WriteLine();
            }

            wr.Close();
            StreamReader read = new StreamReader("D:/test.in");
            var arr = read.ReadLine().Split(' ');
            var size = Convert.ToInt32(arr[1]);
            var square = new SquareMatrix(size);
            square.In(read);

            CollectionAssert.AreEqual(square.Matr, sq.Matr);
            Assert.AreEqual(sq.Size, square.Size);
            read.Close();
        }

        [TestMethod]
        public void DiagIn()
        {
            var sq = new DiagonalMatrix(3);
            int h = 0;
            var wr = new StreamWriter("D:/test.in");
            wr.WriteLine("1 3 1");
            for (int i = 0; i < 3; i++)
            {
                sq.Diag[i] = h;
                wr.Write("{0} ", sq.Diag[i]);
                h++;
            }
            wr.Close();
            StreamReader read = new StreamReader("D:/test.in");
            var arr = read.ReadLine().Split(' ');
            var size = Convert.ToInt32(arr[1]);
            var diag = new DiagonalMatrix(size);
            diag.In(read);
            CollectionAssert.AreEqual(diag.Diag, sq.Diag);
            Assert.AreEqual(sq.Size, diag.Size);
            read.Close();
        }

        [TestMethod]
        public void TriangleIn()
        {
            var sq = new TriangleMatrix(3);
            int h = 0;
            var wr = new StreamWriter("D:/test.in");
            wr.WriteLine("1 3 1");
            for (int i = 0; i < 6; i++)
            {
                sq.Triangle[i] = h;
                wr.Write("{0} ", sq.Triangle[i]);
                h++;
            }
            wr.Close();
            StreamReader read = new StreamReader("D:/test.in");
            var arr = read.ReadLine().Split(' ');
            var size = Convert.ToInt32(arr[1]);
            var triangle = new TriangleMatrix(size);
            triangle.In(read);
            CollectionAssert.AreEqual(triangle.Triangle, sq.Triangle);
            Assert.AreEqual(sq.Size, triangle.Size);
            read.Close();
        }

        [TestMethod]
        public void SquareOut()
        {
            var sq = new SquareMatrix(3);
            int h = 0;
            sq.Size = 3;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    sq.Matr[i, j] = h;
                    h++;
                }

            }
            StreamWriter wr = new StreamWriter("D:/test.txt");
            sq.Out(wr);
            wr.Close();
            var test = new StreamReader("D:/test.txt");
            var sqq = new StreamReader("D:/square.txt");
            Assert.AreEqual(test.ReadToEnd().GetHashCode(), sqq.ReadToEnd().GetHashCode());
            test.Close();
            sqq.Close();
        }


        [TestMethod]
        public void DiagOut()
        {
            var sq = new DiagonalMatrix(3);
            int h = 0;
            sq.Size = 3;
            for (int i = 0; i < 3; i++)
            {
                sq.Diag[i] = h;
                h++;


            }
            StreamWriter wr = new StreamWriter("D:/test.txt");
            sq.Out(wr);
            wr.Close();
            var test = new StreamReader("D:/test.txt");
            var sqq = new StreamReader("D:/DIag.txt");
            Assert.AreEqual(test.ReadToEnd().GetHashCode(), sqq.ReadToEnd().GetHashCode());
            test.Close();
            sqq.Close();
        }


        [TestMethod]
        public void TriangleOut()
        {
            var sq = new TriangleMatrix(3);
            int h = 0;
            sq.Size = 3;
            for (int i = 0; i < 6; i++)
            {
                sq.Triangle[i] = h;
                h++;
            }
            StreamWriter wr = new StreamWriter("D:/test.txt");
            sq.Out(wr);
            wr.Close();
            var test = new StreamReader("D:/test.txt");
            var sqq = new StreamReader("D:/Triangle.txt");
            Assert.AreEqual(test.ReadToEnd().GetHashCode(), sqq.ReadToEnd().GetHashCode());
            test.Close();
            sqq.Close();
        }

        [TestMethod]
        public void ContainerIn()
        {
            StreamReader read = new StreamReader("D:/OOPContainerInTest.in");
            var count = read.ReadLine();
            var cont = new Container();
            cont.Massiv = new Matrix[Convert.ToInt32(count)];
            for (int i = 0; i < Convert.ToInt32(count); i++)
            {
                var arr = read.ReadLine().Split(' ');
                switch (Convert.ToInt32(arr[0]))
                {
                    case 1:
                        {
                            var square = new SquareMatrix(Convert.ToInt32(arr[1]));
                            for (int j = 0; j < square.Size; j++)
                            {
                                var line = read.ReadLine().Split(' ');
                                for (int k = 0; k < square.Size; k++)
                                {
                                    square.Matr[j, k] = Convert.ToInt32(line[k]);
                                }
                            }
                            switch (Convert.ToInt32(arr[2]))
                            {
                                case 0:
                                    {
                                        square.OutType = FormatOut.row;
                                        break;
                                    }
                                case 1:
                                    {
                                        square.OutType = FormatOut.column;
                                        break;
                                    }
                                case 2:
                                    {
                                        square.OutType = FormatOut.inline;
                                        break;
                                    }
                                default:
                                    break;
                            }
                            cont.Massiv[i] = square as Matrix;
                            cont.Length++;
                            break;
                        }
                    case 0:
                        {
                            var diag = new DiagonalMatrix(Convert.ToInt32(arr[1]));
                            var line = read.ReadLine().Split(' ');
                            for (int j = 0; j < diag.Size; j++)
                            {
                                diag.Diag[j] = Convert.ToInt32(line[j]);
                            }
                            switch (Convert.ToInt32(arr[2]))
                            {
                                case 0:
                                    {
                                        diag.OutType = FormatOut.row;
                                        break;
                                    }
                                case 1:
                                    {
                                        diag.OutType = FormatOut.column;
                                        break;
                                    }
                                case 2:
                                    {
                                        diag.OutType = FormatOut.inline;
                                        break;
                                    }
                                default:
                                    break;
                            }
                            cont.Massiv[i] = diag as Matrix;
                            cont.Length++;
                            break;
                        }
                    case 2:
                        {
                            var triangle = new TriangleMatrix(Convert.ToInt32(arr[1]));
                            var line = read.ReadLine().Split(' ');
                            for (int j = 0; j < triangle.Size*(triangle.Size+1)/2; j++)
                            {
                                triangle.Triangle[j] = Convert.ToInt32(line[j]);
                            }
                            switch (Convert.ToInt32(arr[2]))
                            {
                                case 0:
                                    {
                                        triangle.OutType = FormatOut.row;
                                        break;
                                    }
                                case 1:
                                    {
                                        triangle.OutType = FormatOut.column;
                                        break;
                                    }
                                case 2:
                                    {
                                        triangle.OutType = FormatOut.inline;
                                        break;
                                    }
                                default:
                                    break;
                            }
                            cont.Massiv[i] = triangle as Matrix;
                            cont.Length++;
                            break;
                        }
                    default:
                        break;
                }
            }

            var container = new Container();
            read.Close();
            read = new StreamReader("D:/OOPContainerInTest.in");
            container.In(read);
            //CollectionAssert.AreEqual(cont.Massiv, container.Massiv);
            var t = cont.Equals(container);
            for (int i = 0; i < cont.Length; i++)
            {
                Assert.AreEqual(container.Length,cont.Length);
                Assert.AreEqual(container.Massiv[i].Size, cont.Massiv[i].Size);
                Assert.AreEqual(container.Massiv[i].OutType, cont.Massiv[i].OutType);
                if(cont.Massiv[i] is SquareMatrix)
                {
                    CollectionAssert.AreEqual(((SquareMatrix)(cont.Massiv[i])).Matr, ((SquareMatrix)(container.Massiv[i])).Matr);
                }
                else
                {
                    if (cont.Massiv[i] is DiagonalMatrix)
                    {
                        CollectionAssert.AreEqual(((DiagonalMatrix)(cont.Massiv[i])).Diag, ((DiagonalMatrix)(container.Massiv[i])).Diag);
                    }
                    else
                    {
                        CollectionAssert.AreEqual(((TriangleMatrix)(cont.Massiv[i])).Triangle, ((TriangleMatrix)(container.Massiv[i])).Triangle);
                    }
                }
            }
            read.Close();
        }

        [TestMethod]
        public void ContainerOut()
        {
            StreamReader read = new StreamReader("D:/OOPContainerInTest.in");           
            var cont = new Container();
            cont.In(read);
            read.Close();
            var write = new StreamWriter("D:/OOPContainerOutTest.txt");
            cont.Out(write);
            write.Close();
            var sr1 = new StreamReader("D:/OOPContainerOutTest.txt");
            var sr2 = new StreamReader("D:/OOPContainerOut.txt");
            Assert.AreEqual(sr1.ReadToEnd().GetHashCode(), sr2.ReadToEnd().GetHashCode());
            sr1.Close();
            sr2.Close();

        }

        [TestMethod]
        public void ContainerSort()
        {
            StreamReader read = new StreamReader("D:/OOPContainerInTest.in");
            var cont = new Container();
            cont.In(read);
            read.Close();
            read = new StreamReader("D:/OOPContainerSorted.in");
            var sortedcont = new Container();
            sortedcont.In(read);
            read.Close();
            cont.Sort();
            for (int i = 0; i < cont.Length; i++)
            {
                Assert.AreEqual(sortedcont.Length, cont.Length);
                Assert.AreEqual(sortedcont.Massiv[i].Size, cont.Massiv[i].Size);
                Assert.AreEqual(sortedcont.Massiv[i].OutType, cont.Massiv[i].OutType);
                if (cont.Massiv[i] is SquareMatrix)
                {
                    CollectionAssert.AreEqual(((SquareMatrix)(cont.Massiv[i])).Matr, ((SquareMatrix)(sortedcont.Massiv[i])).Matr);
                }
                else
                {
                    if (cont.Massiv[i] is DiagonalMatrix)
                    {
                        CollectionAssert.AreEqual(((DiagonalMatrix)(cont.Massiv[i])).Diag, ((DiagonalMatrix)(sortedcont.Massiv[i])).Diag);
                    }
                    else
                    {
                        CollectionAssert.AreEqual(((TriangleMatrix)(cont.Massiv[i])).Triangle, ((TriangleMatrix)(sortedcont.Massiv[i])).Triangle);
                    }
                }
            }
        }

        [TestMethod]
        public void ContainerFiltered()
        {

            StreamReader read = new StreamReader("D:/OOPContainerInTest.in");
            var cont = new Container();
            cont.In(read);
            read.Close();
            var write = new StreamWriter("D:/OOPCOntinerFilteredTest.txt");
            cont.FilteredOut(write);
            write.Close();
            read = new StreamReader("D:/OOPCOntinerFiltered.txt");
            var read1 = new StreamReader("D:/OOPCOntinerFilteredTest.txt");
            Assert.AreEqual(read.ReadToEnd().GetHashCode(), read1.ReadToEnd().GetHashCode());
            read.Close();
            read1.Close();
        }
    }
}
